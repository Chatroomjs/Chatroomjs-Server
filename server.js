var restify = require('restify');

var authentication = require('./app/routes/authentication');
var stormpath = require('./app/services/stormpath');
var room = require('./app/routes/room');

var server = function () {
    restify.CORS.ALLOW_HEADERS.push('x-access-token');

    this.stormpath = null;

    this.app = restify.createServer();
    this.app.use(restify.CORS());
    this.app.use(restify.bodyParser());

    // FIX RESTIFY PREFLIGHT BUG
    this.app.on('MethodNotAllowed', function (request, response) {
        if (request.method.toUpperCase() === 'OPTIONS') {
            response.header('Access-Control-Allow-Credentials', true);
            response.header('Access-Control-Allow-Headers',
                restify.CORS.ALLOW_HEADERS.join(', '));
            response.header('Access-Control-Allow-Methods',
                'GET, POST, PUT, DELETE, OPTIONS');
            response.header('Access-Control-Allow-Origin', request.headers.origin);
            response.header('Access-Control-Max-Age', 0);
            response.header('Content-type', 'text/plain charset=UTF-8');
            response.header('Content-length', 0);

            response.send(204);
        } else {
            response.send(new restify.MethodNotAllowedError());
        }
    });
}

server.prototype.listen = function (port, callback) {
    this.stormpath = new stormpath(function () {

        var mids = require('./app/middlewares/middlewares')(this.stormpath);
        
        room(this.app, this.stormpath, mids.isAuthenticated);
        authentication(this.app, this.stormpath);

        this.app.listen(port, callback);

    }.bind(this))
}

module.exports = server;