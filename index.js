var RESTServer = require('./server');

function startServer() {
    var server = new RESTServer();
    server.listen(8080, function() {
        console.log('magic happens at 8080');
    });

    return server;
}

startServer();

module.exports = startServer;