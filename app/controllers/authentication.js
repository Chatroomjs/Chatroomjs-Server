
function authentication() {

    function signUp(account, stormpath, callback) {
        stormpath.application.createAccount(account, function (err, account) {
            if (err) {
                callback(err.userMessage, 400);
            } else {
                callback('Created account for the user ' + account.fullName);
            }
        });
    }

    function login(user, stormpath, callback) {
        stormpath.application.authenticateAccount(user, function (err, authResult) {
            if (err) {
                callback(err.userMessage, 400);
            } else {
                authResult.getAccount(function (err, account) {
                    stormpath.oAuthAuthenticator.authenticate(user, function (err, oAuthResult) {
                        if (err) {
                            callback(err.userMessage, 400);
                        } else {
                            var accessToken = oAuthResult.accessTokenResponse.access_token;
                            callback({
                                account: account.href,
                                token: accessToken
                            });
                        }
                    })
                });
            }
        });
    }

    return {
        signUp: signUp,
        login: login,
    }

}

module.exports = authentication;