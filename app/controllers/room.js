var OpenTok = require('../services/opentok');
var Database = require('../database/database');

var session = function () {

    function tryJoinRoom(roomid, callback) {
        var database = new Database();

        database.get(roomid, function (sessionId) {
            if (sessionId) {
                getToken(sessionId, callback);
            } else {
                createNewSession(roomid, callback);
            }
        });
    }

    function createNewSession(roomid, callback) {
        var opentok = new OpenTok();
        opentok.createSession(function (err, session) {
            if (err) {
                return callback(err, 400);
            }
            var database = new Database();
            database.set(roomid, session.sessionId);
            getToken(session.sessionId, callback)
        });
    }

    function getToken(sessionId, callback) {
        var opentok = new OpenTok();      
        token = opentok.generateToken(sessionId, callback);
    }

    function sendSignal (options, callback){
        var opentok = new OpenTok();
        opentok.sendSignal(options.message, options.sessionId ,function(data, response){
            callback(data);
        });
    }

    return {
        tryJoinRoom: tryJoinRoom,
        sendSignal: sendSignal
    }
}

module.exports = session;