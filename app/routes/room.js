var room = require('../controllers/room')();

function roomRoutes(server, stormpath, authenticate) {

    var authenticate = authenticate;

    server.get('/joinroom/:roomid', authenticate, function (req, res, next) {
        if(req.params.roomid === ""){
            return res.send(400, "room id cant be a empty value");
        }
        room.tryJoinRoom(req.params.roomid ,function (token, status) {
            if (status) {
                res.send(status || 200, message);
            }
            return res.send(token);
        });
    });

    server.post('/sendsignal', authenticate, function(req, res,next){
        if(!req.params.message){
            return res.send(400, 'you must provide a message');
        }
        if(!req.params.sessionId){
            return res.send(400, 'you must provide a sessionId');           
        }

        room.sendSignal(req.params, function(err, result){
            if(err) {
                return res.send(400, err);
            }
            return res.send(200, result);
        });
    });
}

module.exports = roomRoutes;
