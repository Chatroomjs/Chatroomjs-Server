var authentication = require('../controllers/authentication')();

function authenticationRoutes(server, stormpath) {
    server.post('/signup', function (req, res, next) {
        var account = req.params;
        authentication.signUp(account, stormpath ,function (message, status) {
            res.send(status || 200, message);
            return next();
        })
    });

    server.post('/login', function (req, res, next) {
        var user = req.params;
        authentication.login(user, stormpath ,function (message, status) {
            res.send(status || 200, message);
            return next();
        })
    });
}

module.exports = authenticationRoutes;