var stormpathSDK = require('stormpath');
var config = require('./config')

var stormpath = function (callback) {
    this.client = new stormpathSDK.Client(config.stormpath);
    this.application = null;
    this.oAuthAuthenticator = null;
    this.JWTAuthenticator = null;
    this.onStart = callback;

    this.client.on('ready', function () {
        this.getApplication(function (application) {
            this.application = application;
            this.oAuthAuthenticator = new stormpathSDK.OAuthPasswordGrantRequestAuthenticator(application);
            this.JWTAuthenticator = new stormpathSDK.JwtAuthenticator(application);
            this.onStart();
            
        }.bind(this));
    }.bind(this));

}

stormpath.prototype.getApplication = function (callback) {
    this.client.getApplications({ name: config.stormpath.applicationName }, function (err, applications) {
        if (err) {
            callback(err.userMessage, 400);
        } else {
            callback(applications.items[0]);
        }
    });
}

module.exports = stormpath;