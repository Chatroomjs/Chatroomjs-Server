var opentokRef = require('opentok');
var config = require('./config');
var rest = require('node-rest-client').Client;
var util = require('util');

var opentok = function() {
    this.client = this.createClient();
    this.restResource = 'http://api.opentok.com/v2/project/%s/session/%s/signal';
}

opentok.prototype.createClient = function(){
    return new opentokRef(config.openTok.apiKey, config.openTok.secret);
}

opentok.prototype.createSession = function(callback) {
    this.client.createSession({media: 'routed'}, callback);
}

opentok.prototype.generateToken = function(sessionId, callback) {
    var token = this.client.generateToken(sessionId);
    callback({token: token, sessionId: sessionId, apiKey: config.openTok.apiKey});
}

opentok.prototype.sendSignal = function(message, sessionId, callback) {
    var restCli = new rest();
    restCli.post(util.format(this.restResource, config.openTok.apiKey, sessionId), {
        data: { message: message },
        headers: { 
            "Content-Type": "application/json",
            "X-TB-PARTNER-AUTH": util.format('%s:%s', config.openTok.apiKey, config.openTok.secret)  
        }
    }, callback);
}

module.exports = opentok;