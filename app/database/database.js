// to use this in windows, follow this instructions https://stackoverflow.com/questions/6476945/how-do-i-run-redis-on-windows/20200022#20200022
var redis = require('redis');

var database = function(){
    this.client = null;
}

database.prototype.connect = function(callback){
    this.client = redis.createClient();

    this.client.on('error', function(err){
        console.log(err);
    });

    this.client.on('ready', function(){
        callback();
    });
}

database.prototype.quit = function(){
    if(this.client) {
        this.client.quit();
        this.client = null;
    }
}

database.prototype.set = function(key, value){
    this.connect(function(){
        this.client.set(key, value, redis.print);
        this.quit();  
    }.bind(this));
}

database.prototype.get = function(key, callback){
    this.connect(function(){
        this.client.get(key, function(err, result){
            if(err) {
                return console.log(err);
            }
            callback(result);
            this.quit();
        }.bind(this));
    }.bind(this));
}

module.exports = database;