function middlewares(stormpath) {

    function isAuthenticated(req, res, next) {
        var token = req.headers['x-access-token'];
        stormpath.JWTAuthenticator.authenticate(token, function(err, authResult) {
            if (err) {
                return res.send(401, err);
            }
            return next();
        });
    }

    function permitCORS(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "x-access-token, Accept-Encoding, Accept-Language");
        res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
        return next();
    }

    function waitStormpath(req, res, next) {
        if (stormpath.application == null) {
            setTimeout(function() {
                return next();
            }, 8000);
        } else {
            return next();
        }
    }

    return {
        isAuthenticated: isAuthenticated,
        permitCORS: permitCORS,
        waitStormpath: waitStormpath
    }
}

module.exports = middlewares;