var server = require('../index')();
var should = require('should');
var config = require('./config')();
var request = require('supertest')(server.app);

describe('# POST /signup', function () {
    it('Tests the access to StormPath by passing an invalid password', function (done) {
        this.timeout(100000);
        request.post('/signup')
            .field('givenName', config.invalidUser.givenName)
            .field('surname', config.invalidUser.surname)
            .field('username', config.invalidUser.username)
            .field('email', config.invalidUser.email)
            .field('password', config.invalidUser.password)
            .set('Content-Type', config.urlencoded)
            .expect(function (res) {
                res.body.should.equal('Password requires at least 1 uppercase character.');
            }).expect(400, done);
    });
});

describe('# POST /login', function () {
    it('Tests the successfull login', function (done) {
        this.timeout(6000);
        request.post('/login')
            .field('username', config.validlogin.username)
            .field('password', config.validlogin.password)
            .set('Content-Type', config.urlencoded)
            .expect(function (res) { res.body.account.should.startWith('https://api.stormpath.com'); })
            .expect(200, done);

    });

    it('Tests the login failed', function (done) {
        this.timeout(6000);
        request.post('/login')
            .field('username', config.invalidUser.username)
            .field('password', config.invalidUser.password)
            .set('Content-Type', config.urlencoded)
            .expect(function (res) { res.body.should.equal('Invalid username or password.') })
            .expect(400, done);

    });
});
