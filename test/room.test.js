var server = require('../index');
var request = require('supertest')(server);
var should = require('should');
var config = require('./config')();


describe('# GET /joinroom', function () {
    it('Tests if cant join a room without authentication', function (done) {
        this.timeout(6000);
        request.get('/joinroom/roomname')
            .expect(401, done); // 401 - Unauthorized
    });

    it('Tests if can create a session while logged in', function (done) {
        this.timeout(10000);
        request.post('/login')
            .field('username', config.validlogin.username)
            .field('password', config.validlogin.password)
            .set('Content-Type', config.urlencoded)
            .end(function (err, res) {
                request.get('/joinroom/roomname')
                    .set('x-access-token', res.body.token)
                    .set('Content-Type', config.urlencoded)
                    .expect(200, done);
            });
    });

    it('Tests if cant create a room with no name', function (done) {
        this.timeout(10000);
        request.post('/login')
            .field('username', config.validlogin.username)
            .field('password', config.validlogin.password)
            .set('Content-Type', config.urlencoded)
            .end(function (err, res) {
                request.get('/joinroom/')
                    .set('x-access-token', res.body.token)
                    .set('Content-Type', config.urlencoded)
                    .expect(400, done);
            });
    });
});
