var server = require('../index');
var request = require('supertest')(server);
var should = require('should');
var config = require('./config')();

describe('# POST /sendsignal', function () {
    it('Tests if cant send signal while logged off', function (done) {
        this.timeout(10000);
        request.post('/sendsignal')
            .field('message', config.signal.message)
            .field('sessionId', config.signal.sessionId)
            .set('Content-Type', config.json)
            .expect(401, done);
    });

    it('Tests if cant send signal without a message', function (done) {
        this.timeout(10000);
        request.post('/login')
            .field('username', config.validlogin.username)
            .field('password', config.validlogin.password)
            .set('Content-Type', config.urlencoded)
            .end(function (err, res) {
                request.post('/sendsignal')
                    .field('message', '')
                    .field('sessionId', config.signal.sessionId)
                    .set('x-access-token', res.body.token)
                    .set('Content-Type', config.json)
                    .expect(400, done);
            });
    });

    it('Tests if cant send signal without a session ID', function (done) {
        this.timeout(10000);
        request.post('/login')
            .field('username', config.validlogin.username)
            .field('password', config.validlogin.password)
            .set('Content-Type', config.urlencoded)
            .end(function (err, res) {
                request.post('/sendsignal')
                    .field('message', config.signal.message)
                    .field('sessionId', '')
                    .set('x-access-token', res.body.token)
                    .set('Content-Type', config.json)
                    .expect(400, done);
            });
    });

    it('Tests if can send signal while authenticated and providing all infos', function (done) {
        this.timeout(100000);
        request.post('/login')
            .field('username', config.validlogin.username)
            .field('password', config.validlogin.password)
            .set('Content-Type', config.urlencoded)
            .end(function (err, res) {
                request.post('/sendsignal')
                    .field('message', config.signal.message)
                    .field('sessionId', config.signal.sessionId)
                    .set('x-access-token', res.body.token)
                    .set('Content-Type', config.json)
                    .expect(function(res){
                        // can send signal, but an error is received due to not having any connection on the session
                        console.log(res.body);
                        res.body.should.startWith("Not found. No clients are actively connected to the OpenTok session.");
                        done();
                    });
            });
    });
});